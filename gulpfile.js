

var gulp            = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins         = gulpLoadPlugins({
                          pattern: ['gulp-*', 'gulp.*'],
                          replaceString: /^gulp(-|\.)/,
                          rename: {
                            'gulp-concat-sourcemap'    : 'concatSourceMap',
                            'gulp-merge-media-queries' : 'mmq',
                            'gulp-concat-css'          : 'concatcss',
                            'gulp-ruby-sass'           : 'rubysass',
                            'gulp-minify-css'          : 'minifycss',
                            'gulp-minify-inline'       : 'minifyInline',
                            'gulp-tslint-stylish'      : 'tslintStylish',
                            'gulp-html-replace'        : 'htmlreplace',
                            'gulp-ng-annotate'         : 'ngAnnotate',
                            'gulp-cache-bust'          : 'cachebust'
                          }
                        }),
    rubysass        = require('gulp-ruby-sass'),
    pngquant        = require('imagemin-pngquant'),
    tslint          = require('gulp-tslint'),
    es              = require('event-stream'),
    del             = require('del'),
    path            = require('path'),
    watch           = require('gulp-watch'),
    sequence        = require('run-sequence'),
    yesno           = require('yesno'),
    gutil           = require('gulp-util');



// Set production to true
var isBuild    = false,
    sassStyle  = 'expanded',
    sourceMap  = true,
    logCMQ     = true,
    clean      = [],
    action     = 'dev',
    tasks      = [
                  'html',
                  'scss',
                  'js',
                  'ts',
                  'fallback',
                  'img',
                  'videos',
                  'fonts',
                  'config',
                  'actions'
                 ];


if ( gutil.env.build === true) {
  // If --build is used

  isBuild    = true;
  sassStyle  = 'compressed';
  sourceMap  = false;
  logCMQ     = false;
  clean      = ['del'];
  action     = 'build';
  tasks      = [
                'html',
                'css',
                'concat-js-dstry',
                'fallback',
                'img',
                'videos',
                'fonts',
                'config',
                'humans',
                'actions'
               ];

  console.log(
    ' ' + '\n' +
    'Build started'
  );

} else if (gutil.env.release === true) {
  // If --release is used

  isBuild    = true;
  sassStyle  = 'compressed';
  sourceMap  = false;
  logCMQ     = false;
  clean      = ['del'];
  action     = 'release';
  tasks      = [
                'html',
                'css',
                'concat-js-dstry',
                'fallback',
                'img',
                'videos',
                'fonts',
                'config',
                'humans',
                'actions'
               ];

  console.log(
    ' ' + '\n' +
    'Release started'
  );


} else {

  console.log(
    ' ' + '\n' +
    'Development started'
  );

}

var pkg = require('./package.json');
// Print this to the scss and js on build
var banner = [
  '',
  ' ',
  '/**',
  ' * Version: <%= pkg.version %>',
  ' * Author',
  ' * <%= pkg.author %> [black-marked.com]',
  ' * Copyright (c) 2015 black-marked',
  ' * All rights reserved',
  '**/',
  ' ',
  ''
].join('\n');




// ------------------------------------------------------------------------------------------------------------------ //
// Setting up environment
gulp.task('setup', ['del'], function (cb) {

  return gulp.src('*.js', {read: false})
    .pipe(plugins.shell( 'bundler install'))
    .pipe(plugins.notify('Environment is setup'), cb);
});
// Setting up environment
// ------------------------------------------------------------------------------------------------------------------ //


// -------------------------------------------------------------------------- //
// GLOBS
var paths = {

  dev_base    : 'dev/',
  dev_html    : 'dev/html/',
  dev_scss    : 'dev/scss/',
  dev_ts      : 'dev/ts/',
  dev_js      : 'dev/js/',
  dev_img     : 'dev/img/',
  dev_vid     : 'dev/video/',
  dev_fonts   : 'dev/type/',
  dev_config  : 'dev/config/',
  dev_fallback: 'dev/fallback/',
  dev_actions : 'dev/action/',

  prod_base    : './www/',
  prod_css     : './www/css/',
  prod_js      : './www/js/',
  prod_img     : './www/img/',
  prod_vid     : './www/video/',
  prod_fonts   : './www/type/',
  prod_fallback: './www/fallback/',
  prod_actions : './www/action/'
};



// -------------------------------------------------------------------------- //
// Del

gulp.task('del', function (cb) {

  console.log(
    ' ' + '\n' +
    ' Clean Started'
  );

  plugins.cached.caches = {};
  del( paths.prod_base, cb );
});






// -------------------------------------------------------------------------- //
// Lint
var paths_lint = {

  ts: [
    paths.dev_ts + '**/*.ts',
    '!' + paths.dev_ts + 'definitions/*.d.ts'
  ]

};

gulp.task('lint', function (cb) {

  console.log(
    ' ' + '\n' +
    'TS lint'
  );

  return gulp.src(paths_lint.ts)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: Typescript Lint Error')}))
    .pipe(isBuild ? gutil.noop() : plugins.print())
    .pipe(isBuild ? gutil.noop() : plugins.cached('typescript-lint-cache'))
    .pipe(tslint())
    .pipe(plugins.tslint.report('tslintStylish'))
    .pipe(plugins.plumber.stop(), cb);
});


// -------------------------------------------------------------------------- //
// Scipts
var pathsTS = {

  js: [
    paths.dev_js + 'jquery-3.0.0.min.js',
    paths.dev_js + 'angular.js',
    paths.dev_js + '*.js'
  ],

  ts: [
    paths.dev_ts + 'config/*.config.ts',
    paths.dev_ts + '**/*.ts',
    '!' + paths.dev_ts + 'definitions/*.d.ts'
  ],

  fallbacks: [
    paths.dev_base + 'fallback/*.js'
  ],

  js_build: [
    paths.prod_js + 'libs.tmp.js',
    paths.prod_js + 'main.tmp.js',
    paths.prod_js + '*.tmp.js'
  ],

  dstry: paths.prod_js + '*.tmp.js'

};




gulp.task('js', function (cb) {

  console.log(
    ' ' + '\n' +
    'Javascript Started'
  );

  return gulp.src(pathsTS.js)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: JS Error')}))
    .pipe(isBuild ? gutil.noop() : plugins.cached('js-cache'))
    .pipe(isBuild ? gutil.noop() : plugins.remember('js-cache'))
    .pipe(isBuild ? gutil.noop() : plugins.print())
    .pipe(isBuild ? gutil.noop() : plugins.sourcemaps.init())
    .pipe(isBuild ? plugins.concat('libs.min.js') : plugins.concatSourceMap('libs.min.js'))
    .pipe(isBuild ? gutil.noop() : plugins.sourcemaps.write())
    .pipe(isBuild ? plugins.ngAnnotate() : gutil.noop())
    .pipe(isBuild ? plugins.uglify({
      compress: {
        drop_console: true
      }
    }) : gutil.noop())
    .pipe(plugins.size())
    .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : gutil.noop())
    .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : gutil.noop())
    .pipe(isBuild ? plugins.rename({ suffix: '.tmp' }) : gutil.noop())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_js))
    .pipe(plugins.livereload(), cb);
});


gulp.task('ts', ['lint'], function (cb) {

  console.log(
    ' ' + '\n' +
    'Typescript Started'
  );

  return gulp.src(pathsTS.ts)
    .pipe(plugins.plumber({
      errorHandler: plugins.notify.onError('Error: TS error')
    }))
    .pipe(isBuild ? gutil.noop() : plugins.cached('ts-cache'))
    .pipe(isBuild ? gutil.noop() : plugins.remember('ts-cache'))
    .pipe(isBuild ? gutil.noop() : plugins.print())
    .pipe(isBuild ? gutil.noop() : plugins.sourcemaps.init())
    .pipe(plugins.typescript({
      target: 'ES5',
      module: 'system',
      sourceMap: true,
      moduleResolution: 'node',
      emitDecoratorMetadata: true,
      experimentalDecorators: true,
      removeComments: false,
      noImplicitAny: false,
      // out: 'main.min.js'
    }))
    .pipe(isBuild ? plugins.concat('main.min.js') : plugins.concatSourceMap('main.min.js'))
    .pipe(isBuild ? gutil.noop() : plugins.sourcemaps.write({includeContent: false, sourceRoot: '/'}))
    // .pipe(isBuild ? plugins.replace('$locationProvider.html5Mode(false);', '$locationProvider.html5Mode(true);') : gutil.noop())
    // .pipe(isBuild ? plugins.replace("link: '#", "link: '") : gutil.noop())
    .pipe(isBuild ? plugins.uglify({
      compress: {
        drop_console: true
      }
    }) : gutil.noop())
    .pipe(plugins.size())
    .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : gutil.noop())
    .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : gutil.noop())
    .pipe(plugins.plumber.stop())
    .pipe(isBuild ? plugins.rename({ suffix: '.tmp' }) : gutil.noop())
    .pipe(gulp.dest(paths.prod_js))
    .pipe(plugins.livereload(), cb);
});




gulp.task('concat-js', ['js', 'ts'], function (cb) {

  gutil.log(gutil.colors.blue('JS Concat'));

  return gulp.src(pathsTS.js_build)
    .pipe(plugins.plumber({
      errorHandler: plugins.notify.onError('Error: JS concat error')
    }))
    .pipe(plugins.concat('black-marked.js'))
    .pipe(plugins.rename({ suffix: '.min' }))
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_js), cb);
});

gulp.task('concat-js-dstry', ['concat-js'], function (cb) {
  del(pathsTS.dstry, cb);
});





gulp.task('fallback', function (cb) {

  console.log(
    ' ' + '\n' +
    'Javascript Started'
  );

  return gulp.src(pathsTS.fallbacks)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: Javascript Error')}))
    .pipe(isBuild ? plugins.uglify({
      compress: {
        drop_console: true
      }
    }) : gutil.noop())
    .pipe(isBuild ? gutil.noop() : plugins.print())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_fallback))
    .pipe(plugins.livereload(), cb);
});



// -------------------------------------------------------------------------- //
// SCSS
var pathsSCSS = {

  scss_master: paths.dev_scss + 'black-marked.scss',

  scss_watch: [
    paths.dev_scss + '*.scss',
    paths.dev_scss + '**/*.scss'
  ],

  css_master: paths.prod_css + 'black-marked.css'

};



gulp.task('scss', function (cb) {

  console.log(
    ' ' + '\n' +
    'SCSS Started'
  );


  return rubysass(pathsSCSS.scss_master, {
    bundleExec: true,
    style: sassStyle,
    sourcemap: sourceMap
  })
  .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: SCSS Lint Error')}))
  .on('error', function (err) {
    console.error('Error!', err.message);
  })
  .pipe(isBuild ? gutil.noop() : plugins.sourcemaps.write())
  .pipe(plugins.size())
  .pipe(isBuild ? gutil.noop() : plugins.mmq({log: logCMQ}))
  .pipe(plugins.plumber.stop())
  .pipe(gulp.dest(paths.prod_css))
  .pipe(plugins.livereload(), cb);

});



gulp.task('css', ['scss'], function (cb) {

  console.log(
    ' ' + '\n' +
    'CSS Started'
  );

  return gulp.src(pathsSCSS.css_master)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: CSS Lint Error')}))
    .pipe(plugins.autoprefixer({
      browsers: [
        'last 2 versions',
      ],
      cascade: false
    }))
    .pipe(plugins.mmq({log: logCMQ}))
    .pipe(isBuild ? plugins.minifycss() : gutil.noop())
    .pipe(isBuild ? plugins.header(banner, { pkg : pkg }) : gutil.noop())
    .pipe(isBuild ? plugins.footer(banner, { pkg : pkg }) : gutil.noop())
    .pipe(plugins.size())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_css), cb);
});




// -------------------------------------------------------------------------- //
// HTML
var pathsHTML = {

  html: [
    paths.dev_html + '*.html',
    paths.dev_html + '**/*.html',
  ]

};

gulp.task('html', function (cb) {

  console.log(
    ' ' + '\n' +
    'HTML Started'
  );

  return gulp.src(pathsHTML.html)
    .pipe(isBuild ? gutil.noop() : plugins.print())
    .pipe(isBuild ? gutil.noop() : plugins.cached('html-cache'))
    .pipe(isBuild ? plugins.htmlreplace({ 'js': 'js/black-marked.min.js' }) : gutil.noop())
    // .pipe(isBuild ? plugins.replace('href="#', 'href="') : gutil.noop())
    .pipe(plugins.htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(plugins.minifyInline())
    .pipe(isBuild ? plugins.cachebust() :  gutil.noop())
    .pipe(plugins.size())
    .pipe(gulp.dest(paths.prod_base))
    .pipe(plugins.livereload(), cb);

});


// -------------------------------------------------------------------------- //

var pathsIMGS = {

  img: [
    paths.dev_img + '*.*',
    paths.dev_img + '**/*.*',
  ],

  imgWebp: [
    paths.prod_img + '*.*',
    paths.prod_img + '**/*.*',

  ]

};


gulp.task('img-webp', function (cb) {

  console.log(
    ' ' + '\n' +
    'WEBP Started'
  );

  return gulp.src(pathsIMGS.img)
    .pipe(plugins.plumber({errorHandler: plugins.notify.onError('Error: WEBP Error')}))
    .pipe(isBuild ? gutil.noop() : plugins.newer(paths.prod_img))
    .pipe(plugins.webp())
    .pipe(plugins.plumber.stop())
    .pipe(gulp.dest(paths.prod_img))
    .pipe(plugins.livereload(), cb);

});


gulp.task('img', ['img-webp'], function (cb) {

  console.log(
    ' ' + '\n' +
    'IMG Started'
  );

  return gulp.src(pathsIMGS.img)
    .pipe(isBuild ? gutil.noop() : plugins.newer(paths.prod_img))
    .pipe(plugins.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant({quality: '65-80', speed: 4})]
    }))
    .pipe(gulp.dest(paths.prod_img))
    .pipe(plugins.livereload(), cb);

});


// -------------------------------------------------------------------------- //

var pathsFONTS = {

  icons: [
    paths.dev_fonts + '*.*',
    paths.dev_fonts + '**/*.*'
  ]

};

gulp.task('fonts', function (cb) {

  console.log(
    ' ' + '\n' +
    'Fonts Started'
  );

  return gulp.src(pathsFONTS.icons)
    // .pipe(isBuild ? plugins.gzip() : gutil.noop())
    .pipe(gulp.dest(paths.prod_fonts))
    .pipe(plugins.livereload(), cb);

});



// -------------------------------------------------------------------------- //

var pathsCONFIG = {

  config: [
    'dev/config/*.*',
    'dev/config/.*'
  ]

};

gulp.task('config', function (cb) {

  console.log(
    ' ' + '\n' +
    'config Started'
  );

  return gulp.src(pathsCONFIG.config)
    .pipe(gulp.dest(paths.prod_base))
    .pipe(plugins.livereload(), cb);

});


// -------------------------------------------------------------------------- //

var pathsACTIONS = {

  actions: [
    paths.dev_actions + '*.*',
  ],

};

gulp.task('actions', function (cb) {

  console.log(
    ' ' + '\n' +
    'config Started'
  );

  return gulp.src(pathsACTIONS.actions)
    .pipe(gulp.dest(paths.prod_actions))
    .pipe(plugins.livereload(), cb);
});



// -------------------------------------------------------------------------- //


var pathsVIDEOS = {

  vids: [
    paths.dev_vid + '**/*.*'
  ],

};

gulp.task('videos', function (cb) {

  console.log(
    ' ' + '\n' +
    'Videos Started'
  );

  return gulp.src(pathsVIDEOS.vids)
    .pipe(gulp.dest(paths.prod_vid))
    .pipe(plugins.livereload(), cb);
});



// -------------------------------------------------------------------------- //

gulp.task('humans', function (cb) {

  gutil.log(gutil.colors.blue('humans'));

  return gulp.src(paths.dev_html + 'index.html')
    .pipe(plugins.humans({
      team: [
        'Title: Chief Front End Developer || CEO',
        'Name: Mark Schwanethal',
        'Site: black-marked.com',
        'Twitter: @black_marked',
        ''
      ],
      site: [
        'Standards: HTML5, CSS3, AngularJS.',
        'Components: TS, SCSS, HTML5shiv.js, selectivizr.js, modernizr.js.',
        'Software: Sublime 3, Visual studio.',
        'Tools: GulpJS, nodeJS.'
      ],
      note: 'Website designed & built by Mark Schwanethal [black-marked.com].'
    }))
    .pipe(gulp.dest(paths.prod_base));
});



// -------------------------------------------------------------------------- //
// Sitemap
var pathsSITEMAP = {

  sitemap: [
    paths.prod_base + '**/*.html',
    paths.prod_base + 'humans.txt',
    '!' + paths.prod_base + 'googlecd960079c6d738b4.html',
    '!' + paths.prod_base + 'independent/home.html',
    '!' + paths.prod_base + 'directives/*.html'
  ]

};

gulp.task('sitemap', function (cb) {

  console.log(
    ' ' + '\n' +
    'Build complete building the sitemap xml'
  );

  return gulp.src(pathsSITEMAP.sitemap)
    .pipe(plugins.sitemap({
      siteUrl: 'http://black-marked.com'
    }))
    .pipe(gulp.dest('www/'), cb);

});




//  .d8888b.
// d88P  Y88b
// Y88b.
//  "Y888b.   888  888  .d8888b .d8888b .d88b.  .d8888b  .d8888b
//     "Y88b. 888  888 d88P"   d88P"   d8P  Y8b 88K      88K
//       "888 888  888 888     888     88888888 "Y8888b. "Y8888b.
// Y88b  d88P Y88b 888 Y88b.   Y88b.   Y8b.          X88      X88
//  "Y8888P"   "Y88888  "Y8888P "Y8888P "Y8888   88888P'  88888P'





// gulp.task('url', function (cb) {

//   gutil.log(gutil.colors.green(' ' + '\n' + 'URL rewrite' + '\n' + ' '));

//   return gulp.src('', {read: false})
//     .pipe(plugins.notify('Release complete'), cb);
// });




gulp.task('success', /*['url'],*/ function (cb) {

  gutil.log(gutil.colors.green(' ' + '\n' + 'Release complete' + '\n' + ' '));

  return gulp.src('', {read: false})
    .pipe(plugins.notify('Release complete'), cb);
});




// -------------------------------------------------------------------------- //
// Watch
gulp.task('watch', function () {

  plugins.livereload.listen();

  console.log(
    ' ' + '\n' +
    '     [ black-marked ]' + '\n' +
    ' '
  );

  var watcherJS = gulp.watch(pathsTS.js, [
    'js'
  ]);
  watcherJS.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['js-cache'];
      plugins.remember.forgetAll('js-cache');
    }
  });

  var watcherTS = gulp.watch(pathsTS.ts, [
    'ts'
  ]);
  watcherTS.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['ts-cache'];
      plugins.remember.forgetAll('ts-cache');
    }
  });

  var watcherHTML = gulp.watch(pathsHTML.html, [
    'html'
  ]);
  watcherHTML.on('change', function (event) {
    if (event.type === 'deleted') {
      delete plugins.cached.caches['html-cache'];
    }
  });

  // gulp.watch(pathsHTML.html, [
  //   'html'
  // ]);
  gulp.watch(pathsTS.fallbacks, [
    'fallback'
  ]);
  gulp.watch(pathsSCSS.scss_watch, [
    'scss'
  ]);
  gulp.watch(pathsIMGS.img, [
    'img'
  ]);
  gulp.watch(pathsFONTS.icons, [
    'fonts'
  ]);
  gulp.watch(pathsCONFIG.config, [
    'config'
  ]);

  console.log(
    ' ' + '\n' +
    ' ' + '\n' +
    'Watch Started' + '\n' +
    ' ' + '\n' +
    ' '
  );

});


// ****************************************************************************************************************** //
// ****************************************************************************************************************** //
// ****************************************************************************************************************** //


//  8888888b.          888
//  888   Y88b         888
//  888    888         888
//  888   d88P .d88b.  888  .d88b.   8888b.  .d8888b   .d88b.
//  8888888P" d8P  Y8b 888 d8P  Y8b     "88b 88K      d8P  Y8b
//  888 T88b  88888888 888 88888888 .d888888 "Y8888b. 88888888
//  888  T88b Y8b.     888 Y8b.     888  888      X88 Y8b.
//  888   T88b "Y8888  888  "Y8888  "Y888888  88888P'  "Y8888


// ************************************************************************** //
// Pre release
// ************************************************************************** //


gulp.task('confirm', ['setup'], function (cb) {

  yesno.ask('Are you sure you want to perform a release? You should be in a release branch! Y/N', true, function (ok) {

    if (ok) {
      console.log(
        ' ' + '\n' +
        ' Release Started'
      );
      cb();
    } else {
      process.exit();
    }
    return;
  });

});


var versionOption = 'version not set';


// Bumping the version once confirmed
gulp.task('prompt', ['confirm'], function (cb) {
  return gulp.src('*')
    .pipe(plugins.prompt.prompt({
      type: 'checkbox',
      name: 'bump',
      message: 'What type of release would you like to do?',
      choices: ['patch', 'minor', 'major']
    }, function (res) {
      if (res.bump == 'minor' ) {
        versionOption = 'minor';
      } else if ( res.bump == 'major' ) {
        versionOption = 'major';
      } else if ( res.bump == 'patch' ) {
        versionOption = 'patch';
      } else {
        process.exit();
      }
    }));
});



// Bumping the version one confirmed
gulp.task('bump', ['prompt'], function (cb) {
  return gulp.src(['./package.json', './npm-shrinkwrap.json'])
    .pipe(plugins.bump({type: versionOption}))
    .pipe(gulp.dest('./'), cb);
});



// ****************************************************************************************************************** //
// ****************************************************************************************************************** //
// ****************************************************************************************************************** //











// ****************************************************************************************************************** //
// Sequence task running
// ****************************************************************************************************************** //

gulp.task('build', function (cb) {
  sequence('setup', tasks, 'sitemap', cb);
});


gulp.task('dev', function (cb) {
  sequence('setup', tasks, 'watch', cb);
});


gulp.task('release', function (cb) {
  sequence('bump', tasks, 'success', cb);
});



// ****************************************************************************************************************** //
// Sequence task running
// ****************************************************************************************************************** //





// GULP
gulp.task('default', [
  action
]);

