/// <reference path='../config/black.config.ts'/>


projectBlack.controller('MendrCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | mendr');
  Seo.setDesc('An in-depth retrospective showcase of building mendr');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
