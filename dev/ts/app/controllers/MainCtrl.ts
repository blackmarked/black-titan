/// <reference path='../config/black.config.ts'/>
/// <reference path='../../definitions/modernizr.d.ts'/>
/// <reference path='../../definitions/bxSlider.d.ts'/>



projectBlack.controller('MainCtrl', [
  '$scope',
  'SliderStatus',
  '$anchorScroll',
  'AnchorSmoothScroll',
  '$location',
  '$timeout',
function (
  $scope,
  SliderStatus,
  $anchorScroll,
  AnchorSmoothScroll,
  $location,
  $timeout
) {


  var that = this;


  // ------------------------------------------------------------------------ //
  // Scroll to function
  this.scrollToEle = function(id) {
    var rst = $location.hash();

    console.log( $location.url() );
    if ( $location.url() !== '' ) {
      $location.url('/');

      console.log('hit');

      $timeout(function() {
        $location.hash(id);
        $anchorScroll();
        $location.hash(rst);
      }, 300);

      return;
    }



    $location.hash(id);
    $anchorScroll();
    $location.hash(rst);
  };
  // Scroll to function
  // ------------------------------------------------------------------------ //




  $scope.$on('sliderStatusBroadcast', function () {
    $scope.showSlider = SliderStatus.getSliderVisibility();
  });



  $scope.$on('$routeChangeSuccess', function () {

    $(document).ready(function () {


      // ---------------------------------------------------------------------- //
      // Img fallback
      $('img').on('error', function() {

        var src = $(this).attr('src'),
            og  = $(this).attr('data-og'),
            fix = src.replace('webp', og);

        console.warn( src );

        $(this).attr('src', fix);
        console.warn('done');
      });

      // Bug with TS means we cant write this in pure JS >:-|

      // var webp = document.getElementsByTagName('img');

      // webp.onerror = function () {
      //   console.warn('error ' + this);
      // };

      // Img fallback
      // ---------------------------------------------------------------------- //


      // ---------------------------------------------------------------------- //
      // Object fit fallback
      if ( Modernizr.objectfit ) {

        // It works

      } else {

        $('.js--object_fit').each(function () {

          var $this  = $(this),
              imgUrl = $this.prop('src');

          if ( imgUrl ) {
            $this.css('backgroundImage', 'url(' + imgUrl + ')').addClass('object-fit--fallback');
          }

        });

      }
      // Object fit fallback
      // ---------------------------------------------------------------------- //

    });


  });


  // $(document).ready(function(){
  //     console.log('load banner');
  //     $timeout(function () {
  //       $('.bxslider').bxSlider({
  //         easing: 'ease-in-out',
  //         randomStart: true
  //       });
  //     });
  // });




}]);
