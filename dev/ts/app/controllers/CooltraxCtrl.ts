/// <reference path='../config/black.config.ts'/>


projectBlack.controller('CooltraxCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Cooltrax');
  Seo.setDesc('A retrospective showcase of building Cooltrax');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
