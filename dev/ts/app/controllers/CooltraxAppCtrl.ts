/// <reference path='../config/black.config.ts'/>


projectBlack.controller('CooltraxAppCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Cooltrax App');
  Seo.setDesc('A retrospective showcase of building Cooltrax the App');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
