/// <reference path='../config/black.config.ts'/>


projectBlack.controller('EmpireCityCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Empire City');
  Seo.setDesc('A retrospective showcase of building Empire City');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
