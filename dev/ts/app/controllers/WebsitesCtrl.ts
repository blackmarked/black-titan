/// <reference path='../config/black.config.ts'/>


projectBlack.controller('WebsitesCtrl', ['$scope', function ($scope: any) {


  // I am the list of friends to show.
  $scope.websites = [
      {
          name: 'zensation',
          link: '/portfolio/zensation',
          logoPath: 'img/thumbs/zensation',
          imgFallback: 'jpg'
      },
      {
          name: 'runaway pets',
          link: '/portfolio/runaway-pets',
          logoPath: 'img/thumbs/runaway-pets',
          imgFallback: 'jpg'
      },
      {
          name: 'mendr',
          link: '/portfolio/mendr',
          logoPath: 'img/thumbs/mendr',
          imgFallback: 'jpg'
      },
      {
          name: 'tchoidesign',
          link: '/portfolio/tchoidesign',
          logoPath: 'img/thumbs/tchoidesign',
          imgFallback: 'jpg'
      },
      {
          name: 'cooltrax app',
          link: '/portfolio/cooltrax-app',
          logoPath: 'img/thumbs/cooltrax-app',
          imgFallback: 'jpg'
      },
      {
          name: 'cooltrax',
          link: '/portfolio/cooltrax',
          logoPath: 'img/thumbs/cooltrax',
          imgFallback: 'jpg'
      },
      {
          name: 'empire city',
          link: '/portfolio/empire-city',
          logoPath: 'img/thumbs/empire',
          imgFallback: 'jpg'
      },
      {
          name: 'foxwoods',
          link: '/portfolio/foxwoods',
          logoPath: 'img/thumbs/foxwoods',
          imgFallback: 'jpg'
      },
      {
          name: 'betfair',
          link: '/portfolio/betfair',
          logoPath: 'img/thumbs/betfair',
          imgFallback: 'jpg'
      },
      {
          name: 'enracha',
          link: '/portfolio/enracha',
          logoPath: 'img/thumbs/enracha',
          imgFallback: 'jpg'
      },
      {
          name: 'money gaming',
          link: '/portfolio/money-gaming',
          logoPath: 'img/thumbs/money-gaming',
          imgFallback: 'jpg'
      },
      {
          name: 'fiction candy jewllery',
          link: '/portfolio/fiction-candy-jewllery',
          logoPath: 'img/thumbs/fiction-candy',
          imgFallback: 'jpg'
      },
      {
          name: 'kear and ku',
          link: '/portfolio/kear-and-ku',
          logoPath: 'img/thumbs/kear-and-ku',
          imgFallback: 'jpg'
      }






  ];




}]);
