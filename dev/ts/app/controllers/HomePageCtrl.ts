/// <reference path='../config/black.config.ts'/>


projectBlack.controller('HomePageCtrl', [
  '$scope',
  'SliderStatus',
  'Seo',
function (
  $scope,
  SliderStatus,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Web/ App design & development');
  Seo.setDesc('Sydney, Australia iOS, Android & Windows applications & responsive website design/ development');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //



  $scope.filterLimit = 6;
  $scope.homepageView = true;

  SliderStatus.setSliderVisibility(true);

  $scope.$on('$locationChangeStart', function( event ) {
    SliderStatus.setSliderVisibility(false);
  });
  // $scope.$on('$routeChangeSuccess', function () {

  // ---------------------------------------------------------------------- //
  // Calculate height of page for hero
  var heroImg    = document.getElementById('hero-img'),
      body       = document.body,
      html       = document.documentElement;

  var height = html.clientHeight;

  heroImg.style.minHeight = height + 'px';
  // Calculate height of page for hero
  // ---------------------------------------------------------------------- //

  // });


}]);
