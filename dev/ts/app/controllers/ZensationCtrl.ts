/// <reference path='../config/black.config.ts'/>


projectBlack.controller('ZensationCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Zensation');
  Seo.setDesc('A retrospective showcase of building Zensation');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
