/// <reference path='../config/black.config.ts'/>


projectBlack.controller('EnrachaCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Enracha');
  Seo.setDesc('A retrospective showcase of building Enracha');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
