/// <reference path='../config/black.config.ts'/>


projectBlack.controller('RunawayPetsCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Runaway Pets');
  Seo.setDesc('An in-depth retrospective showcase of building Runaway Pets');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
