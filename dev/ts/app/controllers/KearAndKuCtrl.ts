/// <reference path='../config/black.config.ts'/>


projectBlack.controller('KearAndKuCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Kear and Ku');
  Seo.setDesc('A retrospective showcase of building Kear and Ku');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
