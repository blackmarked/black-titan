/// <reference path='../config/black.config.ts'/>


projectBlack.controller('ContactCtrl', [
  '$scope',
  '$http',
  '$timeout',
  'Seo',
function (
  $scope,
  $http,
  $timeout,
  Seo
) {




  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Contact me');
  Seo.setDesc('Interested in working with me? Contact me here');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //





  $scope.formData = {};

  $scope.submitForm = function(isValid) {


    // check to make sure the form is completely valid
    if (isValid) {
      console.warn('our form is amazing');

      // --------------------------------------- //
      // Dialog interaction
      $scope.dialogVisible = true;
      $scope.dialog.title = 'Hey ' + $scope.formData.name;
      $scope.dialog.paragraph = 'Thanks for the message, I`ll be in contact soon!';
      $timeout(function () {
        $scope.dialogVisible = false;
      }, 5000);
      // Dialog interaction
      // --------------------------------------- //

      $http({
        method: 'POST',
        url: 'http://black-marked.com/action/contact.php',
        data: $.param($scope.formData),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
      .success(function(data) {
        console.log(data);

        $scope.formData = '';
        $scope.contactForm.$setPristine();

      });

    } else {

      console.warn('our form is fucked');

    }

  };

}]);
