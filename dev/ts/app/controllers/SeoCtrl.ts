/// <reference path='../config/black.config.ts'/>

projectBlack.controller('SeoCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {

  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  $scope.$on('seoBroadcast', function () {
    $scope.seoTitle = Seo.getTitle();
    $scope.seoDesc  = Seo.getDesc();

    console.log($scope.seoTitle + ' ' + $scope.seoDesc);
  });
  // Broadcast SEO
  // ---------------------------------------------------------------------- //

}]);
