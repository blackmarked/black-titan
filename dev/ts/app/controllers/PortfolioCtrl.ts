/// <reference path='../config/black.config.ts'/>


projectBlack.controller('PortfolioCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  $scope.portfolioView = true;

  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Portfolio');
  Seo.setDesc('Showcase of app & website work');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
