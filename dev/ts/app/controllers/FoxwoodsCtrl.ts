/// <reference path='../config/black.config.ts'/>


projectBlack.controller('FoxwoodsCtrl', [
  '$scope',
  'Seo',
function (
  $scope,
  Seo
) {


  // ---------------------------------------------------------------------- //
  // Broadcast SEO
  Seo.setTitle('Black marked | Foxwoods');
  Seo.setDesc('A retrospective showcase of building Foxwoods');
  // Broadcast SEO
  // ---------------------------------------------------------------------- //


}]);
