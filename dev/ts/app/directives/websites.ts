/// <reference path='../config/black.config.ts'/>



projectBlack.directive('websites', function () {
  return {
    restrict: 'A',
    replace: true,
    templateUrl: '/directives/portfolio-tiles.html',
    controller: ['$scope', function ($scope) {
      // Nothing yet
    }]
  };
});
