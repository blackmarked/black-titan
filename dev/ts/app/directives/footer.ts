/// <reference path='../config/black.config.ts'/>



projectBlack.directive('footer', function () {
  return {
    restrict: 'A',
    replace: true,
    templateUrl: '/directives/footer.html',
    controller: ['$scope', '$filter', function ($scope, $filter) {
      // Nothing yet
    }]
  };
});
