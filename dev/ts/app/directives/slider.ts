/// <reference path='../config/black.config.ts'/>
/// <reference path='../../definitions/bxSlider.d.ts'/>



// projectBlack.directive('bxSlider', function () {
//   return {
//     restrict: 'A',
//     link: function(scope, element, attrs: any) {
//       console.log('hit hit hit');
//       if ( element.css ) {
//         $(element).bxSlider(scope.$eval(attrs.bxSlider));
//       }
//     }
//   };
// });



projectBlack.directive('bxSlider', function () {
  return {
    restrict: 'A',
    replace: true,
    templateUrl: '/directives/slider.html',
    controllerAs: 'slider',
    controller: [
      '$scope',
      'SliderStatus',
    function (
      $scope,
      SliderStatus
    ) {


      var that = this;

      $('.bxslider').bxSlider({
        easing: 'ease-in-out',
        auto: true,
        pause: 8000
      });

      // if ( SliderStatus.getSliderStatus() === false ) {
      //   console.log('Slider not yet loaded');


      //   SliderStatus.setSliderStatus(true);

      // } else {
      //   console.log('Slider already loaded');
      //   return;

      // }


    }]
  };
});

