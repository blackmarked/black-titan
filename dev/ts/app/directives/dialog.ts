/// <reference path='../config/black.config.ts'/>



projectBlack.directive('dialog', function () {
  return {
    restrict: 'A',
    replace: true,
    templateUrl: '/directives/dialog.html',
    controllerAs: 'dialog',
    controller: ['$scope', '$filter', function ($scope, $filter) {
      // Nothing yet
    }]
  };
});
