/// <reference path='../../definitions/angular.d.ts'/>


var projectBlack = angular.module('projectBlack', [
  'ngRoute',
  'ngAnimate',
  'angular-google-analytics'
]);


projectBlack.config([
  '$routeProvider',
  '$locationProvider',
function(
  $routeProvider,
  $locationProvider
) {

  $routeProvider
    .when('/', {
      templateUrl: 'independent/home.html',
      controller: 'HomePageCtrl'
    })
    .when('/landing', {
      templateUrl: 'independent/landing.html'
    })
    .when('/portfolio', {
      templateUrl: 'independent/portfolio.html',
      controller: 'PortfolioCtrl'
    })
    .when('/about', {
      templateUrl: 'independent/about.html',
      controller: 'AboutCtrl'
    })
    .when('/contact', {
      templateUrl: 'independent/contact.html',
      controller: 'ContactCtrl'
    })

    .when('/portfolio/zensation', {
      templateUrl: 'websites/zensation.html',
      controller: 'ZensationCtrl'
    })
    .when('/portfolio/runaway-pets', {
      templateUrl: 'websites/runaway-pets.html',
      controller: 'RunawayPetsCtrl'
    })
    .when('/portfolio/mendr', {
      templateUrl: 'websites/mendr.html',
      controller: 'MendrCtrl'
    })
    .when('/portfolio/tchoidesign', {
      templateUrl: 'websites/tchoidesign.html',
      controller: 'TchoiDesignCtrl'
    })
    .when('/portfolio/cooltrax-app', {
      templateUrl: 'websites/cooltrax-app.html',
      controller: 'CooltraxAppCtrl'
    })
    .when('/portfolio/cooltrax', {
      templateUrl: 'websites/cooltrax.html',
      controller: 'CooltraxCtrl'
    })
    .when('/portfolio/empire-city', {
      templateUrl: 'websites/empire-city.html',
      controller: 'EmpireCityCtrl'
    })
    .when('/portfolio/foxwoods', {
      templateUrl: 'websites/foxwoods.html',
      controller: 'FoxwoodsCtrl'
    })
    .when('/portfolio/betfair', {
      templateUrl: 'websites/betfair.html',
      controller: 'BetfairCtrl'
    })
    .when('/portfolio/enracha', {
      templateUrl: 'websites/enracha.html',
      controller: 'EnrachaCtrl'
    })
    .when('/portfolio/money-gaming', {
      templateUrl: 'websites/money-gaming.html',
      controller: 'MoneyGamingCtrl'
    })
    .when('/portfolio/fiction-candy-jewllery', {
      templateUrl: 'websites/fiction-candy-jewllery.html',
      controller: 'FictionCandyJewlleryCtrl'
    })
    .when('/portfolio/kear-and-ku', {
      templateUrl: 'websites/kear-and-ku.html',
      controller: 'KearAndKuCtrl'
    })

    ;

  $locationProvider.html5Mode(true);

}]);


projectBlack.config([
  'AnalyticsProvider',
function (
  AnalyticsProvider
) {

  AnalyticsProvider.setAccount('UA-36611030-2');

}]);
