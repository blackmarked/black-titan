/// <reference path='../config/black.config.ts'/>


projectBlack.service('Seo', [
  '$rootScope',
function (
  $rootScope
) {

  var seoTitle = '',
      seoDesc  = '';


  return {

    broadcastVar: function () {
      $rootScope.$broadcast('seoBroadcast');
    },


    getTitle: function () {
      return seoTitle;
    },

    getDesc: function () {
      return seoDesc;
    },


    setTitle: function (value: string) {
      seoTitle = value;
      this.broadcastVar();
    },

    setDesc: function (value: string) {
      seoDesc = value;
      this.broadcastVar();
    }

  };

}]);
