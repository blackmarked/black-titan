/// <reference path='../config/black.config.ts'/>

// Service to find out weather the slider has loaded or not
projectBlack.service('SliderStatus', [
  '$rootScope',
function (
  $rootScope
) {

  var sliderLoaded: boolean = false,
      sliderVisible: boolean = false;


  return {

    broadcastVar: function () {
      $rootScope.$broadcast('sliderStatusBroadcast');
    },


    getSliderStatus: function () {
      return sliderLoaded;
    },

    setSliderStatus: function (value: boolean) {
      sliderLoaded = value;
      this.broadcastVar();
    },

    getSliderVisibility: function () {
      return sliderVisible;
    },

    setSliderVisibility: function (value: boolean) {
      sliderVisible = value;
      this.broadcastVar();
    }

  };

}]);
